runtime! ftplugin/man.vim  " Use vim's builtin man plugin (use :Man printf)

syntax on
set t_Co=256          " force terminal colours
set mouse=v           " interactively use the mouse when in visual mode
set hlsearch          " highlight all matching patterns
set autoindent        " autoindent
set nowrap            " no line wrapping
set expandtab         " tabs become spaces
set cursorline        " highlight line that cursor is on
set wildmenu          " tab autocomplete shows commands on the statusline
set splitbelow        " by default a split goes below
set splitright        " by default a vertical split goes on the right
set laststatus=2      " always show status bar
set softtabstop=4     " soft tabs represent 4 spaces
set scrolloff=7       " keep spacing at 5 to top/bottom of screen
set timeoutlen=250    " Make exiting escape mode faster
set pastetoggle=<F2>  " Use F2 to toggle paste mode
set directory=$HOME/.vim/swapfiles//
highlight CursorLine term=bold cterm=bold ctermbg=234
highlight ColorColumn ctermbg=235
highlight Search ctermbg=238
highlight LineNr ctermfg=239
highlight VertSplit ctermfg=0
highlight SpellBad ctermfg=red
filetype plugin indent on  " filetype specific indentation
let g:airline_extensions = []
let g:airline_powerline_fonts = 1

" Leader shortcuts
let g:mapleader = "\<space>"          " Use <space> as a leader
nnoremap <leader>h :noh<CR>           " No highlight
nnoremap <leader>s :set spell!<CR>    " Toggle spelling
nnoremap <leader>e :Ex<CR>            " Open explorer
if $WAYLAND_DISPLAY == ""
  " Copy contents of register " into clipboard
  nnoremap <leader>c :let @+=@"<CR>
else
  nnoremap <leader>c :call system("wl-copy", @")<CR>
endif

" auto relative line numbers
set number
augroup numbertoggle
  autocmd!
  autocmd BufEnter,WinEnter * set relativenumber
  autocmd WinLeave * set norelativenumber
augroup END

" Highlight margin in active file only
augroup BgHighlight
  autocmd!
  autocmd BufEnter,WinEnter * set colorcolumn=80
  autocmd WinLeave * set colorcolumn=0
augroup END

" Set local filetype settings
augroup MyFileTypes
  autocmd!
  autocmd FileType c,go,make,rust setlocal noexpandtab softtabstop=0 tabstop=2 shiftwidth=2
  autocmd FileType fugitive setlocal noexpandtab tabstop=2
  autocmd FileType gitcommit setlocal spell
  autocmd FileType javascript,json setlocal softtabstop=0 tabstop=2
  autocmd FileType markdown setlocal spell
  autocmd FileType plaintex setlocal spell
  autocmd Filetype python setlocal formatoptions+=r
  autocmd Filetype python setlocal omnifunc=syntaxcomplete#Complete  " TODO: integrate with tab completion?
  autocmd FileType sh setlocal softtabstop=2
  autocmd FileType tex setlocal spell
  autocmd FileType yaml setlocal softtabstop=2 tabstop=2
  autocmd BufNewFile,BufRead *.toml set syntax=config
  autocmd BufNewFile,BufRead *.sol set syntax=java

  " Define some filetype-specific insert-mode shortcuts
  autocmd Filetype gitcommit inoremap <buffer> <Esc>c [ci skip]
  autocmd Filetype python inoremap <buffer> <Esc>m if __name__ == "__main__":<CR>main()
  autocmd Filetype python inoremap <buffer> <Esc>p print()<Esc>i
  autocmd Filetype python inoremap <buffer> <Esc>i def __init__(self):
  autocmd Filetype python inoremap <buffer> <Esc>t breakpoint()
  autocmd Filetype python inoremap <buffer> <Esc>d """"""<Esc><Left><Left>i
  autocmd Filetype rust inoremap <buffer> <Esc>p println!("{}");<Esc>3<Left>i
  autocmd Filetype rust inoremap <buffer> <Esc>t #[test]
augroup END

if $TERM == "foot"
  " Foot terminal requires this for Alt-key key mappings because there's a
  " specific bug when using foot with vim. NB we would actually have to
  " redefine all of the shortcuts above, but I've only done the ones I'll use
  let &t_TI = "\<Esc>[>4;2m"
  autocmd Filetype gitcommit inoremap <buffer> <A-c> [ci skip]
  autocmd Filetype python inoremap <buffer> <A-m> if __name__ == "__main__":<CR>main()
  autocmd Filetype rust inoremap <buffer> <A-p> println!("{}");<Esc>3<Left>i
  autocmd Filetype rust inoremap <buffer> <A-t> #[test]
endif

" Aliases for commonly made mistakes
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Wq wq
cnoreabbrev Vs vs
cnoreabbrev GSt Gst
cnoreabbrev GST Gst
cnoreabbrev GS Gst

" Create scratch files
command! -nargs=1 S e /tmp/tmp.<args>

" {En,De}crypt using GPG
" TODO: Add optionally setting algorithm
function! Gpg(direction)
  if a:direction == "encrypt"
    let l:cmd = "--symmetric"
  else
    let l:cmd = "--quiet --decrypt"
  endif
  :exec "%!gpg --batch --armour " . l:cmd . " --passphrase " . inputsecret("Passphrase: ")
endfunction
command! GPGEncrypt :call Gpg("encrypt")
command! GPGDecrypt :call Gpg("decrypt")

" Auto formatting via <leader><space>
nnoremap <leader><space> :call Format()<CR>
command! StripTrailingSpaces %s/\s\+$//e | noh | exec "normal \<c-o>"
command! FormatCpp keepjumps %!clang-format --style=LLVM
command! FormatCSV keepjumps %!csv -t -s,
command! FormatJSON keepjumps %!jq
command! FormatMarkdown keepjumps %!mdformat --wrap 80 -
command! FormatPython keepjumps %!black -q -
command! FormatRust keepjumps %!rustfmt --emit stdout
let g:formatters = {
  \"cpp": "FormatCpp",
  \"csv": "FormatCSV",
  \"json": "FormatJSON",
  \"markdown": "FormatMarkdown",
  \"python": "FormatPython",
  \"rust": "FormatRust",
\}
function! Format()
  let l:pos = getpos('.')[1:]
  exe "silent ".get(g:formatters, &ft, "StripTrailingSpaces")
  call cursor(l:pos)
endfunction

" Arrow key mappings
"   * alt-{[,]} -> {previous,next} tab respectively
"   * alt-{h,j,k,l}    -> move to {left,lower,upper,right} buffer
if has("linux")
  nnoremap <Esc>[1;3D <C-w><Left>
  nnoremap <Esc>[1;3C <C-w><Right>
  nnoremap <Esc>[1;3A <C-w><Up>
  nnoremap <Esc>[1;3B <C-w><Down>
elseif has('osx')
  nnoremap ˙ <C-w><Left>
  nnoremap ¬ <C-w><Right>
  nnoremap ˚ <C-w><Up>
  nnoremap ∆ <C-w><Down>
else
  nnoremap <Esc>h <C-w><Left>
  nnoremap <Esc>l <C-w><Right>
  nnoremap <Esc>k <C-w><Up>
  nnoremap <Esc>j <C-w><Down>
endif

" Fix Ctrl-Arrow keys on some systems
map! <ESC>[1;5D <C-Left>
map! <ESC>[1;5C <C-Right>

" Tab completion
" Inserts tab at beginning of line, uses completion if not at beginning
function! InsertTabWrapper()
  let col = col('.') - 1
  if !col || getline('.')[col - 1] !~ '\k'
    return "\<Tab>"
  else
    return "\<C-p>"
  endif
endfunction
inoremap <Tab> <C-r>=InsertTabWrapper()<CR>
inoremap <S-Tab> <C-n>


" Automatically put trailing parenthesis on a new line and indent
function! IndentParenthesesWrapper()
  let col = col('.') - 1
  if col && match(getline('.')[col-1:col], '()\|[]\|{}') == 0
    return "\<CR>\<Tab>\<CR>\<up>\<end>"
  else
    return "\<CR>"
  endif
endfunction
inoremap <CR> <C-R>=IndentParenthesesWrapper()<CR>


" Global var for inline comment characters
let g:inlines = {}
for mapping in [
    \["#", ["", "conf", "sh", "yaml", "zsh", "perl", "python", "toml", "ruby", "r", "make", "gitcommit", "dockerfile", "config", "cfg"]],
    \["//", ["c", "cpp", "dot", "go", "java", "javascript", "php", "rust"]],
    \["--", ["haskell", "lua", "sql"]],
    \["%", ["mat", "tex"]],
    \["\"", ["vim"]],
    \[";", ["dosini"]],
    \["!", ["xdefaults"]]
  \]
  for ft in mapping[1]
    let g:inlines[ft] = mapping[0]
  endfor
endfor

function! NoCommentMsg()
  echoh WarningMsg | echo "No associated comment char with filetype '".&filetype."'" | echoh None
    return ""
endfunction

" Insert a comment using ctrl+/ in normal mode
" TODO: If the previous line contains a comment then we match the indentation.
" ...   This means that when we do 4<ctrl-/> the comments are properly aligned
function! CommentLine(down)
  if !has_key(g:inlines, &filetype)
    :call NoCommentMsg()
    return ""
  endif
  let l:pos = getpos('.')[1:]
  let l:char = g:inlines[&filetype]
  let l:line = getline('.')
  let l:shift = len(l:char) + 1
  if l:line =~ '^\s*'.l:char
    " Uncomment the line
    let l:shift = -l:shift
    call setline('.', substitute(l:line, l:char.' ', '', ''))
  else
    call setline('.', substitute(l:line, '^\s*', '\0'.l:char.' ', ''))
  endif
  if a:down
    let l:pos[0] += 1  " Move cursor down one line
  else
    let l:pos[1] += 1  " Keep cursor in the same position
    let l:pos[1] += l:shift  " Keep cursor in the same position
  endif
  call cursor(l:pos)
endfunction
" NB <C-/> is actually <C-_> on some systems for some reason...
nnoremap <C-_> :call CommentLine(1)<CR>
inoremap <C-_> <Esc>:call CommentLine(0)<CR>i
nnoremap <C-/> :call CommentLine(1)<CR>
inoremap <C-/> <Esc>:call CommentLine(0)<CR>i

" Insert a comment using ctrl+/ in visual mode
" TODO: number of trailing spaces to sub should be determined, this goes for:
" ...   * commenting: if there was no text on the line, dont add trailing spaces
" ...   * uncommenting: if there was no trailing space, we still uncomment
function! VCommentLine() range
  if !has_key(g:inlines, &filetype)
    :call NoCommentMsg()
    return ""
  endif
  let l:char = g:inlines[&filetype]
  let l:pattern = '^\s*\(\s*'.l:char.'\)\@!'
  let l:matches = map(getline("'<", "'>"), "matchlist(v:val, '".l:pattern."')")
  call uniq(l:matches)  " Making matches unique should speed things up
  let l:matchlens = join(map(copy(l:matches), "len(v:val)"), "")
  " If there was a non-0 len match, there was an uncommented line
  if match(l:matchlens, "[^0]") == -1
    " All lines are commented, so we sub all l:char + space with nothing
    let l:subargs = "l:char.' ', ''"
  else
    " 1. get the shortest string in l:matches
    let l:shortest = 9999
    let l:use = "XXX"
    for mtc in l:matches
      if len(mtc) > 0  " There can be some non-matches
        let l:len = len(mtc[0])
        if l:len < l:shortest
          let l:shortest = l:len
          let l:use = mtc[0]
        endif
      endif
    endfor
    " 2. in all lines replace that string with the string + l:char + a space
    let l:subargs = "l:use, l:use.l:char.' '"
  endif
  let exeline = "call setline('.', substitute(getline('.'), ".l:subargs.", ''))"
  echo exeline
  :exe a:firstline.','.a:lastline exeline
endfunction
vnoremap <C-_> :call VCommentLine()<CR>


" Split lines and move to the same column position as before (opposite to
" joining with `J`). This is a bit of an ugly implementation...
nnoremap K :let p=getpos(".")[1:]<CR>r<CR>:let p[0]+=1<CR>:call cursor(p)<CR>

" Use * to search for the visually selected text in visual mode (NB non-magic!)
vnoremap * y/\V<C-R>"<CR>

" Use capital U as redo in normal mode
nnoremap U <C-R>

" Copy the current line into the yank buffer, stripped of leading/trailing space
nnoremap YY :call system("wl-copy", substitute(getline('.'), '\v^\s*', '', ''))<CR>
vnoremap Y :call system("wl-copy", @*)<CR>

" scan the current buffer (.), other loaded buffers (b), unloaded buffers (u)
" buffers from other windows (w), tag completion (t) when completing
set complete=.,b,u,w,t,]

" Keyword list (case-sensitive list of words for auto completion)
set complete+=k~/.vim/keywords.txt


" ========================== FZF commands ==========================

if executable("fzf")

  " Open all files listed under a:items. Go to the lines/columns listed if
  " available (a:items comes from s:rg_to_qf).
  function! s:opener(cmd, items)
    for item in a:items
      let l:cmd = [a:cmd, item.filename]
      if has_key(item, "lnum")
        " Using '<lnum>G' in 'normal!' would add an extra location jump :(
        call insert(l:cmd, "+".item.lnum, 1)
        call add(l:cmd, "| normal! ".item.col)
      else
        call add(l:cmd, "| normal! g`\"")
        " We could do some buffer jump here, but it's easier to just let vim
        " decide :)
      endif
      call add(l:cmd, "| zz")
      exec join(l:cmd, " ")
    endfor
  endfunction

  " Take an `rg --vimgrep` result and split it into filename, lnum, etc.
  function! s:rg_to_qf(line)
    let parts = split(a:line, ':')
    let l:d = {"filename": parts[0]}
    if len(parts) > 1
      let l:d.lnum = parts[1]
      let l:d.col = parts[2]
      let l:d.text = join(parts[3:], "")
    endif
    return l:d
  endfunction

  " Handle the return from `fzf#run`. Perform relevant actions when different
  " control keys are used. Always add to qflist.
  function! s:handler(lines)
    " If no key command was given, the first element is empty, hence we always
    " expect at least 2 elements
    if len(a:lines) < 2 | return | endif
    let cmd = a:lines[0]
    let items = map(a:lines[1:], 's:rg_to_qf(v:val)')

    if cmd == "ctrl-h"
      call s:opener("sp", items)
    elseif cmd == "ctrl-v"
      call s:opener("vs", items)
    elseif cmd == "ctrl-t"
      call s:opener("tabe", items)
    elseif cmd == "ctrl-x"
      for item in uniq(items)
        exec "bdelete!" item.filename
      endfor
    elseif cmd == "ctrl-c"
      " Do nothing (items will be added to qflist anyway)
    else
      call s:opener("e", [items[0]])
    endif
    call setqflist(items)
  endfunction

  let default_options = "--ansi --expect=ctrl-t,ctrl-v,ctrl-x,ctrl-h,ctrl-c ".
                      \ "--multi --bind=ctrl-a:select-all,ctrl-d:deselect-all ".
                      \ "--color hl:68,hl+:110 "

  command! -nargs=+ -complete=file Rg call fzf#run({
    \'source':  'rg --vimgrep '.<q-args>,
    \'sink*':   function('<sid>handler'),
    \'options': default_options.' --delimiter : --nth 4..',
    \'window':  { 'width': 0.9, 'height': 0.6 }
  \})

  command! -nargs=* -complete=dir -bang MyFZF call fzf#run({
    \'sink*':   function('<sid>handler'),
    \'options': default_options,
    \'window':  { 'width': 0.9, 'height': 0.6 }
  \})

  command! FZFBuffers call fzf#run({
    \'source':  map(filter(range(1, bufnr('$')), 'buflisted(v:val)'), 'bufname(v:val)'),
    \'sink*':   function('<sid>handler'),
    \'options': default_options,
    \'window':  { 'width': 0.9, 'height': 0.6 }
  \})

  " Shortcuts
  nnoremap <leader>f :MyFZF<CR>
  nnoremap <leader>b :FZFBuffers<CR>
endif

map <ESC>[1;5D <C-Left>
map <ESC>[1;5C <C-Right>
map! <ESC>[1;5D <C-Left>
map! <ESC>[1;5C <C-Right>
