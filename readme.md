# Vim Configs

## Plugins

### Add New Plugin

```bash
git submodule add <repo-url> pack/plugins/start/<plugin-name>
```

### Update Existing Plugins

```bash
fd -t d -d 1 . pack/plugins/start -x git -C {} pull
```

## Use Vim for Man Pages

A very simple way to use vim as the man pages viewer is to enable man pages in
vim via the builtin plugin (`runtime! ftplugin/man.vim`) and then add this
simple function to a shell dotfile:

```
man () {
  vim -R +"Man $1" +only
}
```

## Key Mappings

`sed -n l` can be used in the terminal to find what key code is being sent to
the terminal when a key is pressed (also note that `^[` is represented as
`<Esc>` in vim)

### `[nv]map` vs. `[nv]noremap`

`noremap` stands for 'non-recursive' mapping, it means that keys will be mapped
to exactly the assigned pattern, not the underlying pattern that the assigned
pattern is itself assigned to. For example, lets say that we have the following
mappings:

```
map a gg
map b a
noremap c a
```

The literal mappings will be `a->gg`, `b->gg`, `c->a`. Because `b` was allowed
to map recursively, it ended up being set to `gg`.

The \[nv\] prefixes simply define whether the mapping is defined within the
scope of \[n\]ormal or \[v\]isual mode (or both if neither is specified.)

## Todo

- Update indentation for multi-line commenting
- Use local <Tab> setting when doing indentation in visual mode

## Cool Commands

Prefix all occurrences of `<pattern>` with incremental numbers (NB only line by
line)

```
:let i=0 | g/\v<pattern>/s/<pattern>/\=i."_".submatch(0)/ | let i=i+1
```

Incrementally replace digits on lines

```
:let i=1 | .,+3g//s/0/\=i/g | let i=i+1
```

Double all digits in a range:

```
:.,+4s/\d/\=(str2float(submatch(0))*2)/g
```

Format a table (over the next 51 lines, assume `|` is the current separator and
desired separator):

```
.,.+51!column -t -s \| -o \|
```

## Change Spelling Language

```
set spelllang=sv # Swedish
set spelllang=en # English
```
